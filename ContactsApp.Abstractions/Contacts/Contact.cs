﻿
namespace ContactsApp.Abstractions.Contacts
{
    public class Contact
    {
        public string FirstName { get; }

        public string LastName { get; }

        public string PhoneNumber { get; }

        public Contact(string firstName, string lastName, string phoneNumber)
        {
            FirstName = firstName;
            LastName = lastName;
            PhoneNumber = phoneNumber;
        }
    }
}
