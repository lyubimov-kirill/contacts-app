﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactsApp.Abstractions.Contacts
{
    public interface IContactsStorage
    {
        Task<IList<Contact>> GetContactsAsync();

        Task UpdateContactsAsync(IEnumerable<Contact> contacts);
    }
}
