﻿using System.Collections.Generic;
using System.Threading.Tasks;

namespace ContactsApp.Abstractions.Contacts
{
    public interface IContactsService
    {
        bool IsPermissionsGranted();

        Task<bool> RequestPermissionsAsync();

        IList<Contact> GetContacts();

        Task<IList<Contact>> GetContactsAsync();
    }
}
