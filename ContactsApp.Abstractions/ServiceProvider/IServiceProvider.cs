﻿namespace ContactsApp.Abstractions.ServiceProvider
{
    // I prefer to use IoC, something like Autofac but using external libraries is strongly prohibited in the task
    public interface IServiceProvider
    {
        void RegisterInstance<T>(T instance);

        T Get<T>();
    }
}
