﻿using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using AddressBook;
using Contacts;
using ContactsApp.Abstractions.Contacts;
using Foundation;

namespace ContactsApp.iOS.Implementations.Contacts
{
    public class ContactsServiceIos : IContactsService
    {
        readonly object _permissionsLocker;

        TaskCompletionSource<bool> _permissionsTcs;

        public ContactsServiceIos()
        {
            _permissionsLocker = new object();
        }

        public IList<Contact> GetContacts()
        {
            var result = new List<Contact>();

            var keys = new[]
            {
                CNContactKey.GivenName,
                CNContactKey.FamilyName,
                CNContactKey.PhoneNumbers
            };

            var store = new CNContactStore();

            var containers = store.GetContainers(null, out NSError error);

            if (containers == null)
                return result;

            foreach (var container in containers)
            {
                using(var predicate = CNContact.GetPredicateForContactsInContainer(container.Identifier))
                {
                    var contacts = store.GetUnifiedContacts(predicate, keys, out NSError contactError);

                    if (contacts == null)
                        continue;

                    foreach(var contact in contacts)
                    {
                        var phone = contact.PhoneNumbers
                            ?.Select(number => number?.Value?.StringValue)
                            ?.FirstOrDefault(phoneString => !string.IsNullOrEmpty(phoneString));

                        result.Add(new Contact(contact.GivenName, contact.FamilyName, phone));
                    }
                }
            }

            return result;
        }

        public Task<IList<Contact>> GetContactsAsync()
        {
            return Task.Run(GetContacts);
        }

        public bool IsPermissionsGranted()
        {
            return ABAddressBook.GetAuthorizationStatus() == ABAuthorizationStatus.Authorized;
        }

        public Task<bool> RequestPermissionsAsync()
        {
            lock(_permissionsLocker)
            {
                if (_permissionsTcs != null)
                    return _permissionsTcs.Task;

                if (IsPermissionsGranted())
                    return Task.FromResult(true);

                var addressBook = ABAddressBook.Create(out NSError error);

                if (error != null && error.Code == (int)ABAddressBookError.OperationNotPermittedByUserError)
                    return Task.FromResult(false);

                _permissionsTcs = new TaskCompletionSource<bool>();

                addressBook.RequestAccess((success, requestError) =>
                {
                    _permissionsTcs.TrySetResult(success);

                    addressBook?.Dispose();
                    addressBook = null;
                });

                return _permissionsTcs.Task;
            }
        }
    }
}
