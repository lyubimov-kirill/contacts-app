﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using ContactsApp.Abstractions.Contacts;
using ContactsApp.Services.Contacts.Extensions;
using ContactsApp.Services.Contacts.Tables;
using SQLite;

namespace ContactsApp.Services.Contacts
{
    public class ContactsStorage : IContactsStorage
    {
        static readonly Lazy<SQLiteAsyncConnection> _db = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        public async Task<IList<Contact>> GetContactsAsync()
        {
            await SetupAsync();

            var contacts = await _db.Value.Table<ContactTable>().ToListAsync();

            return contacts.Select(c => c.ToContact()).ToList();
        }

        public async Task UpdateContactsAsync(IEnumerable<Contact> contacts)
        {
            if (contacts == null || !contacts.Any())
                return;

            await _db.Value.DropTableAsync<ContactTable>();

            await SetupAsync();

            await _db.Value.InsertAllAsync(contacts.Select(c => c.ToTable()));
        }

        async Task SetupAsync()
        {
            await _db.Value.CreateTableAsync<ContactTable>();
        }
    }
}
