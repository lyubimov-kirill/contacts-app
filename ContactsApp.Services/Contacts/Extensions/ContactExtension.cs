﻿using ContactsApp.Abstractions.Contacts;
using ContactsApp.Services.Contacts.Tables;

namespace ContactsApp.Services.Contacts.Extensions
{
    public static class ContactExtension
    {
        public static ContactTable ToTable(this Contact contact)
        {
            return new ContactTable
            {
                FirstName = contact.FirstName,
                LastName = contact.LastName,
                PhoneNumber = contact.PhoneNumber
            };
        }

        public static Contact ToContact(this ContactTable contact)
        {
            return new Contact(contact.FirstName, contact.LastName, contact.PhoneNumber);
        }
    }
}
