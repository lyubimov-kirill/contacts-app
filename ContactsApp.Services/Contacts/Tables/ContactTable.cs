﻿using SQLite;

namespace ContactsApp.Services.Contacts.Tables
{
    [Table("contacts")]
    public class ContactTable
    {
        [PrimaryKey, AutoIncrement]
        public int Index { get; set; }

        [Column("first_name")]
        public string FirstName { get; set; }

        [Column("last_name")]
        public string LastName { get; set; }

        [Column("phone_number")]
        public string PhoneNumber { get; set; }
    }
}
