﻿using System;
using System.Collections.Generic;

namespace ContactsApp.Services.ServiceProvider
{
    public class ServiceProvider : Abstractions.ServiceProvider.IServiceProvider
    {
        readonly Dictionary<Type, object> _instances;

        public ServiceProvider()
        {
            _instances = new Dictionary<Type, object>();
        }

        public void RegisterInstance<T>(T instance)
        {
            _instances[typeof(T)] = instance;
        }

        public T Get<T>()
        {
            var type = typeof(T);

            if (!_instances.TryGetValue(type, out object instance))
                throw new Exception($"{type.FullName} was not registered");

            return (T) instance;
        }
    }
}
