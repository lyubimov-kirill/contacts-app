﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Android.App;
using Android.Content;
using Android.Content.PM;
using Android.Database;
using Android.Provider;
using Android.Runtime;
using Android.Support.V4.App;
using Android.Support.V4.Content;
using ContactsApp.Abstractions.Contacts;
using static Android.Provider.ContactsContract;
using static Android.Provider.ContactsContract.CommonDataKinds;

namespace ContactsApp.Droid.Implementations.Contacts
{
    public class ContactsServiceDroid : IContactsService
    {
        const int PermissionsRequestCode = 101;

        readonly Activity _activity;
        readonly object _permissionsLocker;

        TaskCompletionSource<bool> _permissionTcs;

        public ContactsServiceDroid(Activity activity)
        {
            _activity = activity;

            _permissionsLocker = new object();
        }

        public IList<Contact> GetContacts()
        {
            if (!IsPermissionsGranted())
                throw new Exception($"Permissions are not granted. You must call {nameof(RequestPermissionsAsync)} before to use this method");

            var result = new List<Contact>();

            var projection = new[]
            {
                ContactsContract.Contacts.InterfaceConsts.Id
            };

            var cr = _activity.ContentResolver;

            using (var cursor = cr.Query(ContactsContract.Contacts.ContentUri, projection, null, null, null))
            {
                if (cursor == null || !cursor.MoveToFirst())
                    return result;
                
                do
                {
                    var contact = GetContact(cr, cursor);

                     if (contact != null)
                        result.Add(contact);
                }
                while (cursor.MoveToNext());

                cursor.Close();
            }

            return result;
        }

        public async Task<IList<Contact>> GetContactsAsync()
        {
            return await Task.Run(GetContacts);
        }

        public bool IsPermissionsGranted()
        {
            return ContextCompat.CheckSelfPermission(_activity, Android.Manifest.Permission.ReadContacts) == Permission.Granted;
        }

        public Task<bool> RequestPermissionsAsync()
        {
            lock(_permissionsLocker)
            {
                if (_permissionTcs != null)
                    return _permissionTcs.Task;

                if (IsPermissionsGranted())
                    return Task.FromResult(true);

                _permissionTcs = new TaskCompletionSource<bool>();

                ActivityCompat.RequestPermissions(_activity, new[] { Android.Manifest.Permission.ReadContacts }, PermissionsRequestCode);

                return _permissionTcs.Task;
            }
        }

        public void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            lock(_permissionsLocker)
            {
                if (_permissionTcs == null || requestCode != PermissionsRequestCode)
                    return;

                var result = grantResults.All(p => p == Permission.Granted);

                _permissionTcs.TrySetResult(result);

                _permissionTcs = null;
            }
        }

        Contact GetContact(ContentResolver contentReselver, ICursor cursor)
        {
            var id = cursor.GetString(cursor.GetColumnIndex(ContactsContract.Contacts.InterfaceConsts.Id));
            var phone = GetNumber(contentReselver, id);

            GetName(contentReselver, id, out string givenName, out string familyName);

            return new Contact(givenName, familyName, phone);
        }

        string GetNumber(ContentResolver contentReselver, string id)
        {
            var cursor = contentReselver.Query(Phone.ContentUri, null, $"{Phone.InterfaceConsts.ContactId}=?", new[] { id }, null);

            if (cursor == null || !cursor.MoveToFirst())
                return null;

            return cursor.GetString(cursor.GetColumnIndex(Phone.Number));
        }

        void GetName(ContentResolver contentReselver, string id, out string givenName, out string familyName)
        {
            givenName = null;
            familyName = null;

            var query = $"{Data.InterfaceConsts.Mimetype}=? AND {Phone.InterfaceConsts.ContactId}=?";
            var queryArgs = new[] { StructuredName.ContentItemType, id };

            using (var cursor = contentReselver.Query(Data.ContentUri, null, query, queryArgs, null))
            {
                if (cursor == null || !cursor.MoveToFirst())
                    return;

                givenName = cursor.GetString(cursor.GetColumnIndex(StructuredName.GivenName));
                familyName = cursor.GetString(cursor.GetColumnIndex(StructuredName.FamilyName));
            }
        }
    }
}
