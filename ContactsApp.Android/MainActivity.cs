﻿
using Android.App;
using Android.Content.PM;
using Android.OS;
using Android.Runtime;
using ContactsApp.Abstractions.Contacts;
using ContactsApp.Droid.Implementations.Contacts;
using ContactsApp.Services.ServiceProvider;

namespace ContactsApp.Droid
{
    [Activity(Label = "ContactsApp", Icon = "@mipmap/icon", Theme = "@style/MainTheme", MainLauncher = true, ConfigurationChanges = ConfigChanges.ScreenSize | ConfigChanges.Orientation | ConfigChanges.UiMode | ConfigChanges.ScreenLayout | ConfigChanges.SmallestScreenSize)]
    public class MainActivity : global::Xamarin.Forms.Platform.Android.FormsAppCompatActivity
    {
        readonly ContactsServiceDroid _contactsService;

        public MainActivity()
        {
            _contactsService = new ContactsServiceDroid(this);
        }

        protected override void OnCreate(Bundle savedInstanceState)
        {
            TabLayoutResource = Resource.Layout.Tabbar;
            ToolbarResource = Resource.Layout.Toolbar;

            base.OnCreate(savedInstanceState);

            global::Xamarin.Forms.Forms.Init(this, savedInstanceState);

            var serviceProvider = new ServiceProvider();

            RegisterDependencies(serviceProvider);

            LoadApplication(new App(serviceProvider));
        }

        void RegisterDependencies(ServiceProvider serviceProvider)
        {
            serviceProvider.RegisterInstance<IContactsService>(_contactsService);
        }

        public override void OnRequestPermissionsResult(int requestCode, string[] permissions, [GeneratedEnum] Permission[] grantResults)
        {
            base.OnRequestPermissionsResult(requestCode, permissions, grantResults);

            _contactsService.OnRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }
}