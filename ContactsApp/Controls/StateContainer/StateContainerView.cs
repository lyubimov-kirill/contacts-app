﻿using Xamarin.Forms;

namespace ContactsApp.Controls.StateContainer
{
    [ContentProperty(nameof(NormalView))]
    public class StateContainerView: ContentView
    {
        public static readonly BindableProperty StateProperty = BindableProperty.Create(nameof(State), typeof(States), typeof(StateContainerView), States.Normal, propertyChanged: StateChanged);
        public static readonly BindableProperty NormalViewProperty = BindableProperty.Create(nameof(NormalView), typeof(View), typeof(StateContainerView), null, propertyChanged: ViewStateChange);
        public static readonly BindableProperty LoadingViewProperty = BindableProperty.Create(nameof(LoadingView), typeof(View), typeof(StateContainerView), null, propertyChanged: ViewStateChange);
        public static readonly BindableProperty NoDataViewProperty = BindableProperty.Create(nameof(NoDataView), typeof(View), typeof(StateContainerView), null, propertyChanged: ViewStateChange);
        public static readonly BindableProperty ErrorViewProperty = BindableProperty.Create(nameof(ErrorView), typeof(View), typeof(StateContainerView), null, propertyChanged: ViewStateChange);

        public States State
        {
            get { return (States)GetValue(StateProperty); }
            set { SetValue(StateProperty, value); }
        }

        public View NormalView
        {
            get { return (View)GetValue(NormalViewProperty); }
            set { SetValue(NormalViewProperty, value); }
        }

        public View LoadingView
        {
            get { return (View)GetValue(LoadingViewProperty); }
            set { SetValue(LoadingViewProperty, value); }
        }

        public View NoDataView
        {
            get { return (View)GetValue(NoDataViewProperty); }
            set { SetValue(NoDataViewProperty, value); }
        }

        public View ErrorView
        {
            get { return (View)GetValue(ErrorViewProperty); }
            set { SetValue(ErrorViewProperty, value); }
        }

        private static void StateChanged(BindableObject bindable, object oldValue, object newValue)
        {
            var self = (StateContainerView)bindable;

            self.OnStateChanged((States)newValue);
        }

        private static void ViewStateChange(BindableObject bindable, object oldvalue, object newvalue)
        {
            var self = (StateContainerView) bindable;

            if (self.Content == oldvalue)
                self.OnStateChanged(self.State);
        }

        private void OnStateChanged(States state)
        {
            View newView = null;

            switch (state)
            {
                case States.Normal:
                    newView = NormalView;
                    break;
                case States.Loading:
                    newView = LoadingView;
                    break;
                case States.NoData:
                    newView = NoDataView;
                    break;
                case States.Error:
                    newView = ErrorView;
                    break;
            }

            ChangeContent(newView);
        }

        async void ChangeContent(View view)
        {
            ViewExtensions.CancelAnimations(this);

            if (Content != null)
            {
                await Content.FadeTo(0, 100U);

                Content.IsVisible = false;
            }

            if (view == null)
            {
                Content = null;

                return;
            }

            view.Opacity = 0;

            Content = view;

            view.IsVisible = true;

            await view.FadeTo(1);
        }
    }
}

