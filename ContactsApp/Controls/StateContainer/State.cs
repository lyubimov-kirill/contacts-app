﻿using System;

namespace ContactsApp.Controls.StateContainer
{
    public enum States
    {
        Normal,
        Loading,
        NoData,
        Error
    }
}
