﻿using System;
using System.Reflection;
using System.Threading.Tasks;
using System.Windows.Input;

namespace ContactsApp.Helpers
{
    public class AsyncCommand<T> : ICommand
    {
        readonly Func<T, Task> _executeAsync;
        readonly Func<T, bool> _canExecute;

        bool _isExecuting;

        public event EventHandler CanExecuteChanged;

        public AsyncCommand(Func<Task> execute) : this(execute, null)
        {

        }

        public AsyncCommand(Func<T, Task> execute) : this(execute, null)
        {

        }

        public AsyncCommand(Func<Task> execute, Func<T, bool> canExecute) : this(parameter => execute(), canExecute)
        {

        }

        public AsyncCommand(Func<T, Task> execute, Func<T, bool> canExecute)
        {
            if (execute == null)
                throw new NullReferenceException($"{nameof(execute)} parameter can not be null");

            _executeAsync = execute;
            _canExecute = canExecute;
        }

        public virtual bool CanExecute(object parameter)
        {
            if (_canExecute != null)
            {
                if (IsValidParameter(parameter))
                    return _canExecute.Invoke((T)parameter);
                return false;
            }
            return true;
        }

        public virtual async void Execute(object parameter)
        {
            if (_isExecuting || !IsValidParameter(parameter))
                return;

            _isExecuting = true;

            await _executeAsync.Invoke((T)parameter);

            _isExecuting = false;
        }

        public virtual void ChangeCanExecute()
        {
            CanExecuteChanged?.Invoke(this, EventArgs.Empty);
        }

        private bool IsValidParameter(object o)
        {
            if (o != null)
            {
                return o is T;
            }

            Type typeFromHandle = typeof(T);
            if (Nullable.GetUnderlyingType(typeFromHandle) != null)
            {
                return true;
            }
            return !typeFromHandle.GetTypeInfo().IsValueType;
        }
    }

    public class AsyncCommand : AsyncCommand<object>
    {
        public AsyncCommand(Func<Task> execute) : base(execute)
        {
        }

        public AsyncCommand(Func<object, Task> execute) : base(execute)
        {
        }

        public AsyncCommand(Func<Task> execute, Func<object, bool> canExecute) : base(execute, canExecute)
        {
        }

        public AsyncCommand(Func<object, Task> execute, Func<object, bool> canExecute) : base(execute, canExecute)
        {
        }
    }
}
