﻿using ContactsApp.Abstractions.ServiceProvider;
using ContactsApp.ViewModels;
using Xamarin.Forms;
using Xamarin.Forms.PlatformConfiguration;
using Xamarin.Forms.PlatformConfiguration.iOSSpecific;

namespace ContactsApp.Pages
{
    public partial class MainPage : ContentPage
    {
        public MainPage(IServiceProvider serviceProvider)
        {
            InitializeComponent();

            BindingContext = new MainPageViewModel(serviceProvider);

            On<iOS>().SetUseSafeArea(true);
        }
    }
}
