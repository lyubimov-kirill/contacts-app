﻿using ContactsApp.Abstractions.Contacts;
using ContactsApp.Abstractions.ServiceProvider;
using ContactsApp.Pages;
using ContactsApp.Services.Contacts;
using Xamarin.Forms;

namespace ContactsApp
{
    public partial class App : Application
    {
        public App(IServiceProvider serviceProvider)
        {
            serviceProvider.RegisterInstance<IContactsStorage>(new ContactsStorage());

            InitializeComponent();

            MainPage = new MainPage(serviceProvider);
        }
    }
}
