﻿using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using System.Threading.Tasks;
using System.Windows.Input;
using ContactsApp.Abstractions.Contacts;
using ContactsApp.Abstractions.ServiceProvider;
using ContactsApp.Controls.StateContainer;
using ContactsApp.Helpers;
using ContactsApp.LocalizableResources;

namespace ContactsApp.ViewModels
{
    public class MainPageViewModel : ViewModelBase
    {
        readonly IContactsService _contactsService;
        readonly IContactsStorage _contactsStorage;

        string _loading;
        public string LoadingMessage
        {
            get { return _loading; }
            private set { SetProperty(ref _loading, value); }
        }

        string _errorMessage;
        public string ErrorMessage
        {
            get { return _errorMessage; }
            private set { SetProperty(ref _errorMessage, value); }
        }

        string _noDataMessage;
        public string NoDataMessage
        {
            get { return _noDataMessage; }
            private set { SetProperty(ref _noDataMessage, value); }
        }

        States _currentState;
        public States CurrentState
        {
            get { return _currentState; }
            private set { SetProperty(ref _currentState, value); }
        }

        IList<Contact> _contacts;
        public IList<Contact> Contacts
        {
            get { return _contacts; }
            private set { SetProperty(ref _contacts, value); }
        }

        public ICommand SyncCommand { get; }

        public MainPageViewModel(IServiceProvider serviceProvider)
        {
            _contactsService = serviceProvider.Get<IContactsService>();
            _contactsStorage = serviceProvider.Get<IContactsStorage>();

            Contacts = new ObservableCollection<Contact>();

            SyncCommand = new AsyncCommand(SyncAsync);

            Load();
        }

        async void Load()
        {
            if (CurrentState == States.Loading)
                return;

            LoadingMessage = AppResources.Loading;

            CurrentState = States.Loading;

            // Long loading simulation
            await Task.Delay(2000);

            var contacts = await _contactsStorage.GetContactsAsync();

            Update(contacts);
        }

        async Task SyncAsync()
        {
            if (CurrentState == States.Loading)
                return;

            LoadingMessage = AppResources.Synchronization;

            CurrentState = States.Loading;

            if(!_contactsService.IsPermissionsGranted())
            {
                var isPermissionsGranted = await _contactsService.RequestPermissionsAsync();

                if(!isPermissionsGranted)
                {
                    ErrorMessage = AppResources.PermissionsNotGranted;

                    CurrentState = States.Error;

                    return;
                }
            }

            var contacts = await _contactsService.GetContactsAsync();

            await _contactsStorage.UpdateContactsAsync(contacts);

            // Long loading simulation
            await Task.Delay(2000);

            Update(contacts);
        }

        void Update(IList<Contact> contacts)
        {
            if (!contacts.Any())
            {
                NoDataMessage = AppResources.ContactsNotSynchronizedYet;

                CurrentState = States.NoData;

                return;
            }

            Contacts = contacts;

            CurrentState = States.Normal;
        }
    }
}
